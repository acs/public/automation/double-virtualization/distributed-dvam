# -*- coding: utf-8 -*-
"""
Created on Wed Jun 08 2022

@author: Nikolaus Wirtz
Unit Failure Detection in a Virtual Power Plant
Updates only failure state of units
"""

from ntpath import join
import time
#import datetime
#import math
#import requests
import numpy as np
#import cmath
from numpy import exp
from numpy import abs
from numpy import angle
import psycopg2
import pandas as pd
#import os
import socket

def query_unit_meas(units, timestamp=None):
    """Creates an SQL query to retreive unit measurements.
    Inputs:
    units is a list of VPP units of type string
    timestamp is a timestamp in miliseconds of type bigint
    Output:
    df is the dataframe of unit measurements"""
        
    #Connect to PostgreSQL DB
    con = psycopg2.connect(
        host='localhost',
        dbname='vpp',
        user='pi',
        password='pi')

    #Create cursor object using the cursor() method
    cur = con.cursor()
    
    #Create query
    query = 'SELECT timestamp'
    for j in range(len(units)):
        query = query + ', ' + units[j]
    
    query += ' FROM UNIT_MEAS '
    if timestamp is None:
        query = query + 'ORDER by TIMESTAMP DESC LIMIT 1'
    else:
        query = query + 'WHERE TIMESTAMP > ' + str(timestamp) + ' ORDER by TIMESTAMP DESC'
    
    cur.execute(query)
    data = cur.fetchall()
    df = pd.DataFrame(data=data, columns=['timestamp']+units)
    
    #Commit changes in the database
    con.commit()

    #Close the connection
    con.close()
    
    return df

def query_unit_states():
    """Creates an SQL query to retreive unit states.
    Inputs:
    units is a list of VPP units of type string
    Output:
    unit_active is the list of unit active state
    unit_failed is the list of unit failed state"""
        
    #Connect to PostgreSQL DB
    con = psycopg2.connect(
        host='localhost',
        dbname='vpp',
        user='pi',
        password='pi')

    #Create cursor object using the cursor() method
    cur = con.cursor()
    
    #Create query
    query = 'SELECT * FROM UNIT_STATES ORDER by NAME ASC'
    
    cur.execute(query)
    data = cur.fetchall()
    df = pd.DataFrame(data=data, columns=['NAME', 'ACTIVE', 'FAILED', 'PRIO', 'FLEXIBILITY', 'AVAILABLE_FLEXIBILITY'])
    
    unit_active = list(df.loc[:, 'ACTIVE'])
    unit_failed = list(df.loc[:, 'FAILED'])
    unit_av_flex = list(df.loc[:, 'AVAILABLE_FLEXIBILITY'])

    #Commit changes in the database
    con.commit()

    #Close the connection
    con.close()
    
    return unit_active, unit_failed, unit_av_flex

#if __name__ == "__main__":

print('script started')

units = ['U01', 'U02', 'U03', 'U04', 'U05', 'U06', 'U07', 'U08', 'U09', 'U10']
thresholds = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
unit_failed = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

while True:
    change_flag = False
    unit_failed_old = unit_failed
    
    meas = query_unit_meas(units)
    unit_active, unit_failed, unit_av_flex = query_unit_states()
    
    for i, elrow in meas.iterrows():
        for j in range(len(units)):
            if elrow[j+1] < thresholds[j]:
                unit_failed[j] = 1
            else:
                unit_failed[j] = 0
    
    for i in range(len(unit_failed)):
        if unit_failed[i] is not unit_failed_old[i]:
            change_flag = True

    print(change_flag)
    
    if change_flag:
        NODE_UDP_IP="localhost" 
        NODE_UDP_PORT_send=9001
        
        sock_node_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        unit_state = unit_active+unit_failed+unit_av_flex
        print(unit_state)
        msg = bytearray(unit_state)
        #print(msg)
        sock_node_send.sendto(msg, (NODE_UDP_IP, NODE_UDP_PORT_send))
        
    time.sleep(2)
