# -*- coding: utf-8 -*-
"""
Created on Wed Jun 08 2022

@author: Nikolaus Wirtz
Service Restoration in a Virtual Power Plant
"""

import pandas as pd
import psycopg2
import socket
import time

def query_unit_states():
    """Creates an SQL query to retreive unit states.
    Inputs:
    units is a list of VPP units of type string
    Output:
    unit_active is the list of unit active state
    unit_failed is the list of unit failed state"""
        
    #Connect to PostgreSQL DB
    con = psycopg2.connect(
        host='localhost',
        dbname='vpp',
        user='pi',
        password='pi')
    #Create cursor object using the cursor() method
    cur = con.cursor()
    #Create query
    query = 'SELECT * FROM UNIT_STATES ORDER by NAME ASC'
    cur.execute(query)
    data = cur.fetchall()
    unit_states = pd.DataFrame(data=data, columns=['NAME', 'ACTIVE', 'FAILED', 'PRIO', 'FLEXIBILITY', 'AVAILABLE_FLEXIBILITY'])
    #Commit changes in the database
    con.commit()
    #Close the connection
    con.close()
    
    return unit_states

print('script started')

#Initialize unit status table
unit_names = ['U01', 'U02', 'U03', 'U04', 'U05', 'U06', 'U07', 'U08', 'U09', 'U10']
unit_init = [0] * 10
unit_status = pd.DataFrame({'name':unit_names, 'active':unit_init, 'failed':unit_init, 'prio':unit_init, 'flexibility':unit_init, 'available_flexibility':unit_init})

#Set required flexibility
VPP_flexibility = 500

while True:
    change_flag = False
    unit_active_old = unit_status['active']
    unit_av_flex_old = unit_status['available_flexibility']
    #Get unit state update
    unit_state_update = query_unit_states()
    #Update unit status table
    #Failed state and available flexibility
    for i, elrow in unit_state_update.iterrows():
        unit_status.at[i, 'active'] = elrow[1]
        unit_status.at[i, 'failed'] = elrow[2]
        unit_status.at[i, 'prio'] = elrow[3]
        unit_status.at[i, 'flexibility'] = elrow[4]
        unit_status.at[i, 'available_flexibility'] = (1-elrow[2])*elrow[4]
    
    print(unit_status)
    
    #Determine units to be activated
    activated_flexibility = 0
    unit_status_sorted = unit_status.sort_values('prio')
    for i, elrow in unit_status_sorted.iterrows():
        if activated_flexibility < VPP_flexibility and elrow[2] == 0:
            unit_status_sorted.at[i, 'active'] = 1
            activated_flexibility += elrow[5]
        else:
            unit_status_sorted.at[i, 'active'] = 0
    unit_status = unit_status_sorted.sort_values('name')
    
    for i in range(len(unit_active_old)):
        if unit_active_old[i] != unit_status['active'][i]:
            change_flag = True

    for i in range(len(unit_av_flex_old)):
        if unit_av_flex_old[i] != unit_status['available_flexibility'][i]:
            change_flag = True
                    
    if change_flag:
        
        NODE_UDP_IP="localhost" 
        NODE_UDP_PORT_send=9002
        
        sock_node_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        unit_state = unit_status['active'].tolist() + unit_status['failed'].tolist() + unit_status['available_flexibility'].tolist()
        msg = bytearray(unit_state)
        sock_node_send.sendto(msg, (NODE_UDP_IP, NODE_UDP_PORT_send))
    
    time.sleep(10)
