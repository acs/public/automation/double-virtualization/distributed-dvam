# Double Virtualization with distributed Administration and Management

### Double Virtualization (DV) implementation based on Node-RED

The concept was developed and described in the **_SUCCESS_**[^1] project, which presented an implementation based on **_Calvin_** and **_Node-RED_**.
The concept was adopted in **_DEFENDER_**[^2], where an implementation based on **_Node-RED_** only was provided.
A major shortcoming of the **_DEFENDER_**[^2] implementation, namely the central Administration and Management (A&M) component - a single point of failure - was removed in the implementation presented here, which offers a distributed A&M component based on the **_Raft_** consensus.
The distribution of the A&M component and the related improved DV implementation were developed during the **_PHOENIX_**[^3][^4] project and have been described in public deliverables.

### Installation & Requirements

The project requires:
- **_Python_**
- **_Node.js_**
- **_Node-RED_**
- **_Node Package Manager (npm)_**
- **_PostgreSQL_**

Install the latest version of **_Python 3_** from https://www.python.org/downloads/. Verify that the path to the python executeable got added to the PATH system variable. For that open up the command prompt and use the command `python --version`. It should output the just installed python version.

To install the required software the repository contains a bash script *pi-setup.sh*. The script upgrades the apt-package manager and installs **_Node.js_**, **_Node-RED_**, **_npm_** and **_PostgreSQL_**. To run the script use `sh pi-setup.sh` in the command line from the root directory of the project repository. In case there are no execute permissions set for the files *pi-setup.sh* and *pi-setup-code.py* you have to manually set them first with `chmod +x pi-setup.sh` and `chmod +x pi-setup-code.py`.

Additionally it installs the **_Node-RED-packages_** used by the project:
- *node-red-contrib-buffer-parser*
- *node-red-contrib-grpc*
- *node-red-contrib-mytimeout*
- *node-red-contrib-postgres-variable*
- *node-red-node-pi-gpio*
- *node-red-node-ping*
- *node-red-node-random*
- *node-red-node-rbe*
- *node-red-node-serialport*
- *node-red-node-smooth*
- *node-red-node-tail*

If you already have **_Node-RED_** installed, make sure that there are no conflicting packages while running the script.

### PSQL
Before you can start creating the specific database, you need to create a superuser.
To create a user with the name pi use the commands `sudo su postgres` and `createuser pi -P --interactive`.

Choose a password for your user and make him a superuser. 

Now you can create the database using the data flow in **_Node-RED_**. In the function node for creating the table, you will have to change the credentials according to your previous configuration of the PSQL user **_pi_**. The same credentials must be used in the Python scripts that contain the functions and in the PSQL nodes in Node-RED.
Here you can use the Inject-Button to create a database with the name vpp and also create the necessary tables and initialize them.

### Configuration

After successfully installing the required software, the project can be configured if needed. For the configuration step the repository contains a python script *pi-setup-code.py*. The script takes a flows.json file as input and generates the flows.json files for each device accordingly. The configuration of the project is set in another python file *config.py*.

```python
# Config file for pi-setup-code.py

# Set amount of devices
devices = 6

# Set hostnames
address_prefix = "example-prefix-"
address_suffix = ".example-suffix"
```

The amount of devices and the prefix and suffix of the hostnames can be configured to your likings.

To generate the flows.json-files use `python pi-setup-code.py` in the command line from the root directory of the project.

### Usage

To run the the project the above generated flows.json-files needs to be imported to **_Node-RED_**.
For this open up **_Node-RED_** and click on the Menu in the top right corner and select *Import*.
Choose the respective flows.json-file for the current device and select *Open*.

The code is based on three different flows:
- dv am
- data
- dv asset
- create data

The **data** flow is used to initialize the local storages and the connection to the *PostgresDB* on each device.
The **dv am** flow contains the general algorithm for the Double Virtualisation, while the assignment of the virtual functions is outsourced in the **dv asset** flow.
The **create data** flow is used to generate artificial measurements that are sent to all devices. This flow must only be activated on one of the devices.

First you need to deploy the project once. After this open up the data flow and use the *inject* and *Create*-buttons to create the local storages and the connection to the database.

Lastly, use the *start*-button in the **dv am** flow to run the project.

**This work was partially funded by the H2020 PHOENIX project, contract no. 832989, within the H2020 Framework Program of the European Commission.**

### References

[^1]: "*D2.4 The Resilience by Design Concept V1*"; H2020 SUCCESS;\
  https://success-energy.eu/files/success/Content/Library/Deliverables/700416_deliverable_D2_4.pdf

[^2]: "*A Cyber-Physical Approach to Resilience and Robustness by Design*", Giovanni Di Orio, Guilherme Brito, Pedro Malo, Abhinav Sadu, Nikolaus Wirtz and Antonello Monti;\
  International Journal of Advanced Computer Science and Applications (IJACSA), 11(7), 2020;\
  http://dx.doi.org/10.14569/IJACSA.2020.0110710

[^3]: "*D2.2 Secure and Persistent Communications Layer (Ver. 0)*"; H2020 PHOENIX;\
  https://phoenix-h2020.eu/wp-content/uploads/PHOENIX_D2.2.pdf

[^4]: "*D2.3 Secure and Persistent Communications Layer (Ver. 1)*"; H2020 PHOENIX;\
  https://phoenix-h2020.eu/wp-content/uploads/PHOENIX_D2.3.pdf

