# Config file for pi-setup-code.py

# Set amount of devices
devices = 6

# Set hostnames
address_prefix = "prefix-example-"
address_suffix = ".suffix-example"