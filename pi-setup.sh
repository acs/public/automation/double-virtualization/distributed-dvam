#!/bin/bash

# Change to root directory
cd

# Install Nodejs, Node-RED and npm
echo -e "Installing Node.js, Node-RED and npm\n"
#bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)

#Checking Node.js Install
if ! [ -x "$(command -v node)" ]; then
  echo 'Error: Cannot install Node.js.' >&2
  exit 1
else
  version_nodejs=$(node -v)
fi

#Checking npm Install
if ! [ -x "$(command -v npm)" ]; then
  echo 'Error: Cannot install npm.' >&2
  exit 1
else
  version_npm=$(npm -v)
fi

#Checking Node-RED Install
if ! [ -x "$(command -v node-red)" ]; then
  echo 'Error: Cannot install node-red.' >&2
  exit 1
fi

echo -e "Succesfully installed Node.js:" $version_nodejs
echo -e "Succesfully installed npm:" $version_npm
echo -e "Succesfully installed Node-RED\n"

echo -e "Installing Node-RED Palette\n"
npm init -y

echo -e "Installing node-red-contrib-buffer-parser:"
npm install node-red-contrib-buffer-parser --save

echo -e "Installing node-red-contrib-grpc:"
npm install node-red-contrib-grpc --save

echo -e "Installing node-red-contrib-mytimeout:"
npm install node-red-contrib-mytimeout --save

echo -e "Installing node-red-contrib-postgres-variable:"
npm install node-red-contrib-postgres-variable --save

echo -e "Installing node-red-node-pi-gpio:"
npm install node-red-node-pi-gpio --save

echo -e "Installing node-red-node-ping:"
npm install node-red-node-ping --save

echo -e "Installing node-red-node-random:"
npm install node-red-node-random --save

echo -e "Installing node-red-node-rbe:"
npm install node-red-node-rbe --save

echo -e "Installing node-red-node-serialport:"
npm install node-red-node-serialport --save

echo -e "Installing node-red-node-smooth:"
npm install node-red-node-smooth --save

echo -e "Installing node-red-node-tail:"
npm install node-red-node-tail --save

echo -e "\nInstallation succesful."

# Postgresql

sudo apt-get upgrade
sudo apt-get install postgresql-client
