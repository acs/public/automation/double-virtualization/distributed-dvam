import json
import config

with open('flows.json', 'r') as f:
    json_data = json.load(f)

    for i in range(1,config.devices+1):
        # psql nodes: Namen und server hier setzen
        hostname = config.address_prefix + str(i) + config.address_suffix
        print("\nDevice", i, ": ", hostname)
        
        checklist = {
            'id' : False,
            'tcp' : False
        }

        for item in json_data:
            # set own id and hostname in init-params-node
            if (item['id'] == "8cfc218d.653c98"):
                func = item['func']

                # Set own ID
                index = func.find('global.set("device1.id"')
                print("ID: " , func[index+24:index+30])
                func = func[:index+24] + str(100000+i) + func[index+30:]
                print(func[index:index+32])

                # Set hostname prefix
                if (i == 1):
                    index = func.find('global.set("device1.hostname_prefix"')
                    print("Prefix: ", func[index:index+44])
                    print("Prefix: ", func[index+38:index+41])
                    func = func[:index+38] + str(config.address_prefix) + func[index+41:]
                    print(func[index:index+41+len(str(config.address_prefix))])

                    index = func.find('global.set("device1.hostname_suffix"')
                    print("Suffix now")
                    print(func[index+38:index+41])
                    func = func[:index+38] + str(config.address_suffix) + func[index+41:]
                    print(func[index:index+41+len(str(config.address_suffix))])

                    # Set amount of devices
                    index = func.find('global.set("device1.nodesNum"')

                    print(func[index+30:index+31])
                    func = func[:index+30] + str(config.devices) + func[index+31:]
                    print(func[index:index+33])

                item['func'] = func

                checklist['id'] = True

            # set port for tcp communication
            if (item['id'] == "b5c5283949b29bd5"):
                item['port'] = 9054

                checklist['tcp'] = True
                    
                
        # Save each flows.json if checklist complete
        if (all(checklist.values())):
            print("saving flows-" + hostname + ".json")
            out_filename = "flows-" + hostname + ".json"
            out_file = open(out_filename, "w")
            json.dump(json_data, out_file, indent = 4)
        
            out_file.close()
        else:
            for id in checklist:
                if (checklist[id] == False):
                    print(id)
            
    
